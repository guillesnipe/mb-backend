# MagicBox Backend

## Prerequisites
1 - Install MongoDB. Check the following [MongoDB Installation Guide](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/)

> Note: You can no longer create the `/data/db` folder in the default location, create it in your user folder instead. eg: `~/data/db`. Use this path when initializing your db server.

> Important: You can not longer create the `/data/db` folder in the default location, create the folder in your user folder instead `~/data/db`

## Running the app

1 - Open a new terminal window

2 - Initialize MongoDB in a separate shell using the following command, or just mongod (Based on your settings)

		mongod --dbpath ~/data/db

3 - In a new tab install the dependencies doing `npm install`

4 - Start the mb-backend server by executing:

		npm run dev

5 - Wait for the success message

6 - Now you can proceed starting the application, check the mb-frontend repo instructions