import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';

//MongoDB Models
import Question from './models/Question';

const app = express();
const router = express.Router();

app.use(cors());
app.use(bodyParser.json({limit: '10mb'}));

mongoose.connect('mongodb://localhost:27017/bb-certification', { useNewUrlParser: true });

const connection = mongoose.connection;

connection.once('open', () => {
  console.log('MongoDB database connection established successfully');
})

// Routes

// GET /questions
router.route('/questions').get((req, res) => {
  Question.find((err, questions) => {
    if (err) { console.log(err); }
    else {
      console.log(questions);
      res.json(questions);
    }
  })
})

// GET /questions/:id
router.route('/questions/:id').get((req, res) => {
  Question.findById(req.params.id, (err, question) => {
    if (err) { console.log(err); }
    else {
      res.json(question);
    }
  })
});

// POST /questions/add
router.route('/questions/add').post((req, res) => {
  let question = new Question(req.body);
  question.save()
    .then(question => {
      res.status(200).json({'question':'Added Successfully'});
    })
    .catch(err => {
      res.status(400).send('Failed to create the new record');
    });
});

// POST /questions/import
router.route('/questions/import').post((req, res) => {
  Question.collection.insert(req.body, (err, docs) => {
      if (err) {
          console.log('Error importing questions');
      } else {
          console.info('Questions were stored.', docs.length);
      }
  })
});

// POST /questions/update/:id
router.route('/questions/update/:id').post((req, res) => {
  Question.findById(req.params.id, (err, question) => {
    if (!question) {
      return next(new Error('Could not load document'));
    } else {
      question.itemId = req.body.itemId;
      question.stem = req.body.stem;
      question.optionA = req.body.optionA;
      question.optionB = req.body.optionB;
      question.optionC = req.body.optionC;
      question.optionD = req.body.optionD;
      question.optionE = req.body.optionE;
      question.type = req.body.type;
      question.processedAnswers = req.body.processedAnswers;
      question.exhibitId = req.body.exhibitId;
      question.exhibitPlacement = req.body.exhibitPlacement;
      question.exhibitSplit = req.body.exhibitSplit;
      question.stemImage = req.body.stemImage;
      question.imagePlacement = req.body.imagePlacement;
      question.minOptions = req.body.minOptions;
      question.maxOptions = req.body.maxOptions;
      question.supressOptionLabels = req.body.supressOptionLabels;
      question.stemHeader = req.body.stemHeader;
      question.stemFooter = req.body.stemFooter;

      question.save().then(question => {
        res.json(`Question ${question.itemId} succesfully updated!`);
      }).catch(err => {
        res.status(400).send(`Question ${question.itemId} Update failed: ${err}`);
      });
    }
  });
});

// POST /questions/delete/:id
router.route('/questions/delete/:id').get((req, res) => {
  Question.findByIdAndRemove({_id: req.params.id}, (err, question) => {
    if (err) { res.json(err) }
    else {
      res.json('Removed successfully');
    }
  })
});

// DELETE /questions/delete
router.route('/questions/delete').delete((req, res) => {
  Question.collection.deleteMany({}, (err, obj) => {
    if (err) throw err;
    console.log(obj);
    res.json(obj);
    //connection.close();
  });
});

app.use('/', router);

app.listen(4000, () => console.log(`Express server running on port 4000`));
