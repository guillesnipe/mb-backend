import mongoose from 'mongoose';

const Schema = mongoose.Schema;

let Question = new Schema({
  itemId : {
    type: String
  },
  stem : {
    type: String
  },
  optionA : {
    type: String
  },
  optionB : {
    type: String
  },
  optionC : {
    type: String
  },
  optionD : {
    type: String
  },
  optionE : {
    type: String
  },
  type : {
    type : String,
    default: 'Single'
  },
  processedAnswers : {
    type: String
  },
  exhibitId : {
    type: String
  },
  exhibitPlacement : {
    type: String
  },
  exhibitSplit : {
    type: String
  },
  stemImage : {
    type: String
  },
  imagePlacement : {
    type: String
  },
  minOptions : {
    type: String
  },
  maxOptions : {
    type: String
  },
  supressOptionLabels : {
    type: String
  },
  stemHeader : {
    type: String
  },
  stemFooter : {
    type: String
  }
});

export default mongoose.model('Question', Question);